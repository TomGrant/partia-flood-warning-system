from floodsystem import geo
from floodsystem import stationdata

def stations_river(dictionary, river):
    stations = dictionary[river]

    stations_names = []
    for s in stations:
        stations_names.append(s.name)
    stations_names.sort()
    return stations_names

def run():
    """Requirements for Task 1D"""
    stations = stationdata.build_station_list()

    rivers = geo.rivers_with_station(stations)
    print("Rivers with at least one monitoring station")
    print(len(rivers))
    print("First 10 rivers")
    print(rivers[:10])

    river_dictionary = geo.stations_by_river(stations)

    print("Stations located on: River Aire")
    print(stations_river(river_dictionary, 'River Aire'))

    print("Stations located on: River Cam")
    print(stations_river(river_dictionary, 'River Cam'))

    print("Stations located on: River Thames")
    print(stations_river(river_dictionary, 'River Thames'))


if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()
