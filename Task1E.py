from floodsystem.geo import rivers_by_station_number
from floodsystem import stationdata

def run():
    """Requirements for Task 1E"""
    stations = stationdata.build_station_list()
    stations_on_river = rivers_by_station_number(stations, 9)
    print(stations_on_river)


if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    run()