from datetime import timedelta, datetime
import numpy as np

from floodsystem import stationdata, datafetcher, flood, plot, analysis

def run():
    """Requirements for Task 2G"""

    all_stations = stationdata.build_station_list()
    stationdata.update_water_levels(all_stations)

    risks = analysis.get_town_risks(all_stations)
    print("Highest risk of flooding is at the following towns")
    for (town, risk, _) in risks[::-1]:
        print("Town: {:<30} Risk: {}".format(town, risk))

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()
