"""Unit test for the geo module"""

from floodsystem.tests.fake_stationdata import build_fake_station_list, build_fake_station_river_list

from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance, stations_within_radius, rivers_by_station_number, rivers_with_station, stations_by_river

from functools import partial

def test_stations_by_distance():
    stations = build_fake_station_list()
    stations = stations_by_distance(stations, (52.2043801, 0.1077598))

    names = map(lambda station_data: station_data[0].name, stations)
    assert(next(names) == "Best college")
    assert(next(names) == "Bourton Dickler")
    assert(next(names) == "Atlantic Ocean")
    assert(next(names) == "Texas")


def test_stations_within_radius():
    stations = build_fake_station_list()
    stations_in_radius = partial(stations_within_radius, stations, (52.2043801, 0.1077598))

    stations_radius_tiny = stations_in_radius(1)
    stations_radius_uk = stations_in_radius(1000)

    assert(len(stations_radius_tiny) == 1)
    assert(len(stations_radius_uk) == 2)

def test_rivers_by_station_number():
    stations = build_fake_station_river_list()
    
    rivers = rivers_by_station_number(stations, 2)
    
    assert rivers[0] == ('river1', 30)
    assert rivers[1] == ('river2', 20)
    assert rivers[2] == ('river3', 20)

    assert len(rivers) == 3

def test_stations_by_river():
    stations = build_fake_station_river_list()
    river_dict = stations_by_river(stations)

    assert len(river_dict['river1']) == 30
    assert len(river_dict['river4']) == 10