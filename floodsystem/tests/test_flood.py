from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_highest_rel_level, stations_level_over_treshold

def mk_stations(stations):
    results = []
    for name, level in stations:
        obj = MonitoringStation(0, 0, name, 0, (0, 100), '', '')
        obj.latest_level = level
        results.append(obj)
    return results

def test_stations_highest_relative_level():
    fake_list_highest_lvl = mk_stations([("station1", 12), ("station2", 23), ("station3", 45), ("station4", 83), ("station5", 22), ("station6", 28), ("station7", 67), ("station8", 89), ("station9", 25), ("station10", 123)])

    sorted_fake_list_highest_lvl = mk_stations([("station10", 123), ("station8", 89), ("station4", 83), ("station7", 67), ("station3", 45), ("station6", 28), ("station9", 25), ("station2", 23), ("station5", 22), ("station1", 12)])

    program_out = stations_highest_rel_level(fake_list_highest_lvl, 10)

    assert program_out[0][0].name == sorted_fake_list_highest_lvl[0].name
    assert program_out[5][0].name == sorted_fake_list_highest_lvl[5].name
    assert program_out[-1][0].name == sorted_fake_list_highest_lvl[-1].name

    assert program_out[0][1] == 1.23

def test_stations_level_over_treshold():
    #Also tests station.relative_water_level
    fake_list_highest = mk_stations([("station1", 12), ("station2", 23), ("station3", 45), ("station4", 83), ("station5", 22), ("station6", 28), ("station7", 67), ("station8", 89), ("station9", 25), ("station10", 123)])
    fake_list_highest_lvl = mk_stations([("station1", 12), ("station2", 23), ("station3", 45), ("station4", 83), ("station5", 22), ("station6", 28), ("station7", 67), ("station8", 89), ("station9", 25), ("station10", 123)])

    sorted_fake_list_highest_lvl = mk_stations([("station10", 123), ("station8", 89), ("station4", 83), ("station7", 67), ("station3", 45), ("station6", 28), ("station9", 25), ("station2", 23), ("station5", 22), ("station1", 12)])

    program_out = stations_level_over_treshold(fake_list_highest_lvl, 0.8)

    assert program_out[0][0].name == sorted_fake_list_highest_lvl[0].name

    assert program_out[0][1] == 1.23
    assert len(program_out) == 3