from floodsystem.station import MonitoringStation
from functools import partial
import random

def build_fake_station_list():
    """Build and return a list of all river level monitoring stations
    based data is. Each station is
    represented as a MonitoringStation object.

    The available data for some station is incomplete or not
    available.

    """

    # Build list of MonitoringStation objects
    stations = [
        MonitoringStation(
            station_id = "http://environment.data.gov.uk/flood-monitoring/id/stations/1030TH",
            measure_id = "http://environment.data.gov.uk/flood-monitoring/id/measures/1030TH-level-stage-i-30_min-mASD",
            label = "Texas",
            coord = (-95.9617081, 29.8159956), #Texas
            typical_range = (0.68, 421),
            river = "Colorado River",
            town = "Austin"),
        MonitoringStation(
            station_id = "http://environment.data.gov.uk/flood-monitoring/id/stations/1029TH",
            measure_id = "http://environment.data.gov.uk/flood-monitoring/id/measures/1029TH-level-stage-i-15_min-mASD",
            label = "Bourton Dickler",
            coord = (51.874767, -1.740083), #actual coords, UK
            typical_range = (0.065, 0.32),
            river = "River Dikler",
            town = "Little Rissington"),
        MonitoringStation(
            station_id = "http://environment.data.gov.uk/flood-monitoring/id/stations/1011TH",
            measure_id = "http://environment.data.gov.uk/flood-monitoring/id/measures/1011TH-level-stage-i-15_min-mASD",
            label = "Atlantic Ocean",
            coord = (-72.2635034, 19.8429894), #Atlantic Ocean
            typical_range = (0.068, 0.42),
            river = "Atlantic River",
            town = "Atlanta?"),
        MonitoringStation(
            station_id = "http://environment.data.gov.uk/flood-monitoring/id/stations/1011TH",
            measure_id = "http://environment.data.gov.uk/flood-monitoring/id/measures/1011TH-level-stage-i-15_min-mASD",
            label = "Best college",
            coord = (52.2043801, 0.1077598),
            typical_range = (10, 10.1),
            river = "Cam",
            town = "Cambridge"),
        ]

    return stations


def random_station_on_river(river, id):
    return MonitoringStation(
            station_id = id,
            measure_id = f"measure_{id}_test",
            label = f"label_{id}_test",
            coord = (random.random(), random.random()),
            typical_range = (random.random(), random.random()),
            river = river,
            town = "town")

def build_fake_station_river_list():
    return [
        *map(partial(random_station_on_river, "river1"), range(30)),
        *map(partial(random_station_on_river, "river2"), range(20)),
        *map(partial(random_station_on_river, "river3"), range(20)),
        *map(partial(random_station_on_river, "river4"), range(10)),
    ]