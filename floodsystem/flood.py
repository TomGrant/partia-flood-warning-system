def stations_level_over_treshold(stations, tol):
    """Creating an empty list"""
    list_station_over_treshold = []

    for station in stations:
        water_level = station.relative_water_level()
        if water_level == None:
            continue
        if water_level > tol:
            list_station_over_treshold.append((station, station.relative_water_level()))
   
    list_station_over_treshold.sort(key = lambda t: t[1], reverse=True)

    return list_station_over_treshold

def stations_highest_rel_level(stations, N):
    """Creating an empty list"""
    list_highest_lvl = []
    for station in stations:
        water_level = station.relative_water_level()
        if water_level == None:
            continue

        list_highest_lvl.append((station, water_level))

    list_highest_lvl = sorted(list_highest_lvl, key = lambda t: t[1], reverse=True)
    return list_highest_lvl[:N]
