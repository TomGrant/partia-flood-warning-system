import matplotlib
from matplotlib import pyplot as plt
import numpy as np

from datetime import datetime, timedelta

from floodsystem import analysis, utils

def graphical_mode():
    try:
        matplotlib.use( 'tkagg' )
    except:
        pass

def plot_water_levels(station, dates, levels):
    graphical_mode()
    """Creating the dates list"""
    t = dates
    """Creating the levels list"""
    l = levels
    """Plot"""
    plt.plot(t,l)
    
    """Add axis labels, rotate date labels and add plot title"""
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title("Station {}".format(station.name))

    """Display plot"""
    """This makes sure plot does not cut off date labels"""
    plt.tight_layout()  

    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    """ Plots a best fit line representing the dates/ levels history
    """
    graphical_mode()

    # Gen polyfit
    best_fit, offset = analysis.polyfit(dates, levels, p)

    # Gen polyfit
    plt.hlines( station.typical_range, colors='orange', xmin=dates[0], xmax=dates[-1], label = 'Typical range')
    plt.plot( dates, best_fit(utils.dates_to_num(dates, offset)), label = 'Polyfit' )

    # Format chart
    plt.legend()

    plt.xlabel('Date')
    plt.ylabel('Water Level (m)')
    plt.xticks(rotation = 45)
    plt.title(f"Station {station.name}")
    plt.tight_layout()

    plt.show()

def plot_prediction(station, future = timedelta(days=1)):
    """ Plots a best fit line representing the dates/ levels history
    """
    graphical_mode()
    now = datetime.utcnow()
    
    # Gen polyfit
    best_fit, offset = station.polyfit

    # Gen polyfit
    plt.hlines( station.typical_range, colors='orange', xmin=station.dates[-1], xmax=now+future, label = 'Typical range')
    plt.plot( station.dates, best_fit(utils.dates_to_num(station.dates, offset)), label = 'Polyfit' )

    max_reading = max(utils.dates_to_num(station.dates, offset))

    future_dates = np.arange(max_reading, utils.dates_to_num(now + future, offset), 0.001 )
    plt.plot( future_dates-offset, best_fit(future_dates), label = 'Future' )

    # Format chart
    plt.legend()

    plt.xlabel('Date')
    plt.ylabel('Water Level (m)')
    plt.xticks(rotation = 45)
    plt.title(f"Station {station.name}")
    plt.tight_layout()

    plt.show()