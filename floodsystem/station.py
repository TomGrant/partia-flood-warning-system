# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""
from floodsystem import datafetcher, analysis, utils
from datetime import timedelta, datetime

class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town, max_scale=None, max_recorded_range=None):

        self._station_id = station_id
        self._measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self._name = label
        if isinstance(label, list):
            self._name = label[0]

        self._coord = coord
        self._typical_range = typical_range
        self._river = river
        self._town = town

        self._max_scale = max_scale
        self._max_recorded_range = max_recorded_range
        self.latest_level = None

        self.dates, self.levels = [], []
        self.polyfit_range = 0
        self.polyfit = None

    @property
    def measure_id(self):
        return self._measure_id

    @property
    def station_id(self):
        return self._station_id

    @property
    def name(self):
        return self._name

    @property
    def coord(self):
        return self._coord
    
    @property
    def typical_range(self):
        return self._typical_range

    @property
    def river(self):
        return self._river
    
    @property
    def town(self):
        return self._town

    @property
    def max_scale(self):
        return self._max_scale

    @property
    def max_recorded_range(self):
        return self._max_recorded_range

    def __repr__(self):
        return "\n".join([
            "Station name:     {}".format(self.name),
            "   id:            {}".format(self._station_id),
            "   measure id:    {}".format(self._measure_id),
            "   coordinate:    {}".format(self.coord),
            "   town:          {}".format(self.town),
            "   river:         {}".format(self.river),
            "   typical range: {}".format(self.typical_range),
        ])

    def typical_range_consistent(self):
        """Does station typical_range data exist and contain vaild data?
        """
        if not self.typical_range:
            return False
        return self.typical_range[0] < self.typical_range[1]



    def relative_water_level(self):
        """Returns the ratio of the actual level over the typical range
        """
        if not self.typical_range_consistent() or not self.latest_level:
            return None
            
        return float((self.latest_level-self.typical_range[0])/(self.typical_range[1]-self.typical_range[0]))

    def predict(self, time):
        """Predicts the water level at a given time
        """
        return self.polyfit[0](utils.dates_to_num(time) + self.polyfit[1])

    def init_prediction(self, dt=timedelta(days=3)):
        """Fetches and saves a best fit line for level prediction
        """
        if self.polyfit_range == dt:
            return
        self.polyfit_range = dt

        self.dates, self.levels = datafetcher.fetch_measure_levels(self.measure_id, dt)
        if(len(self.dates) == 0 or len(self.levels) == 0):
            print("Invalid station history")
            self.polyfit = (lambda x: self.latest_level, 0)
            return
        self.polyfit = analysis.polyfit(self.dates, self.levels, 4)


def inconsistent_typical_range_stations(stations):
    """Returns a list of stations with inconsistent level data
    """
    return list(filter(
        lambda station: not station.typical_range_consistent(),
        stations
    ))

def consistent_typical_range_stations(stations):
    """Returns a list of stations with consistent level data
    """
    return list(filter(
        lambda station: station.typical_range_consistent(),
        stations
    ))