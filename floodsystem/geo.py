# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from floodsystem.utils import sorted_by_key

import math
from math import sqrt, sin, cos

def coords_to_radians(coord):
    return [math.radians(coord[0]), math.radians(coord[1])]

def haversine_distance(coord1, coord2):
    """Returns the haversine distance beetween 2 sets of coordinates
    https://en.wikipedia.org/wiki/Haversine_formula
    """
    EARTH_RADIUS = 6_371 #km
    lat1, long1 = coords_to_radians(coord1)
    lat2, long2 = coords_to_radians(coord2)
    return 2 * EARTH_RADIUS * math.asin(
        sqrt(
            sin( (lat2 - lat1)/2 ) ** 2

            +

            cos( lat1 ) * cos( lat2 )
            *
            sin( (long2 - long1)/2 ) ** 2
        )
    )


def stations_by_distance(stations, p):
    """Returns a list of (station, distance) tuples sorted by distance
    distance is a float representing the station's distance from coordinate p in km
    """
    return sorted_by_key(
        map(
            lambda station: (station, haversine_distance(p, station.coord)),
            stations
        ),
        1
    )

def stations_within_radius(stations, centre, r):
    """Returns a list of stations within a radius r km of the center point
    """
    return list(filter(
        lambda station: haversine_distance(centre, station.coord) < r,
        stations
    ))

def rivers_with_station(stations):
    """Returns a list of rivers which have stations on them
    """
    rivers=[]
    for station in stations:
        river = station.river

        if river not in rivers:
            rivers.append(river)
    rivers.sort()

    return rivers 

def stations_by_river(stations):
    """Returns a dictionary of rivers which maps the river's name to that stations on them
    """
    river_dict = {}
    for station in stations:
        if station.river not in river_dict.keys():
            river_dict[station.river] = []
        river_dict[station.river].append(station)
    return river_dict

def rivers_by_station_number(stations, N):
    """Returns a tuple of the N rivers with the greatest number of monitoring stations.
    (river: string, count: int)
    """
    stations_per_river = []
    rivers = rivers_with_station(stations)

    for river in rivers:
        nr = 0
        for station in stations:
            if station.river == river:
                nr+=1
        
        stations_per_river.append((river, nr))
    
    result = sorted_by_key(stations_per_river, 1, True)

    for [index, station] in enumerate(result[N:]):
        if result[N-1][1] != station[1]:
            return result[:N+index]

    return result