"""This module contains functions to analyse
station data.

"""
from floodsystem import datafetcher, utils, station

import matplotlib
import matplotlib.pyplot as plt

from datetime import timedelta, datetime

import numpy as np
from collections import defaultdict

def polyfit(dates, levels, p):
    """Returns a model: (function, offset)
    Where function models the relationship between dates and levels
    Internally the model is a polynomial of degree p
    """
    x = matplotlib.dates.date2num(dates)

    coeff = np.polyfit(x - x[0], levels, p)
    return np.poly1d(coeff), -x[0]

def predict_relative_increase(station, past=timedelta(days=3), future=timedelta(days=1)):
    now = datetime.utcnow()
    station.init_prediction(past)

    current_level = station.predict(now)
    future_level = station.predict(now + future)

    return (future_level-current_level)/(station.typical_range[1]-station.typical_range[0])

def predict_relative_increases(stations, past=timedelta(days=3), future=timedelta(days=1)):
    """Predicts the water level increate after time 'future' from the current time
    """
    return utils.sorted_by_key(
        map(
            lambda station: (station, predict_relative_increase(station, past, future)),
            stations
        ),
        1,
        reverse=True
    )

def get_station_risk(station):
    """Gets the numerical risk of a station flooding from 0+
    """
    relative_level = station.relative_water_level()
    if not relative_level:
        return None
    if relative_level < 2:
        return relative_level
    return relative_level + predict_relative_increase(station)/10

def get_station_risks(stations):
    """Gets the sorted numerical risk of each station flooding from 0+
    """
    return utils.sorted_by_key(
        filter(
            lambda station: station[1],
            map(
                lambda station: (station, get_station_risk(station)),
                stations
            )
        ),
        1, reverse=True
    )

def get_town_risks(stations):
    """Returns a list of towns and their corresponding deviation from the mean flood risk
    Ranking is based on:
        Average risk from surrounding stations
            Stations risk is calculated by considering the current relative level and projected level in 1 day
                Level is projected using a ployfit and past data

        The number of standard devitations from the mean flood risk of all sites:
            5: Severe
            3: High
            2: Moderate
            Else: Low
    """
    stations = station.consistent_typical_range_stations(stations)

    towns = defaultdict(lambda: (0, 0))
    for st, risk in get_station_risks(stations):
        town_risks, samples = towns[st.town]
        towns[st.town] = ((town_risks*samples + risk)/(samples+1), samples+1)
    
    # There are massively inaccurate values, ignore these in stats
    sample = utils.sample_data(towns.values())
    std = np.std(sample, axis=0)[0]
    mean = np.mean(sample, axis=0)[0]

    result = []
    for town, (risk, _) in towns.items():
        deviations = (risk - mean)/std
        if deviations > 5:
            risk = 'severe'
        elif deviations > 3:
            risk = 'high'
        elif deviations > 2:
            risk = 'moderate'
        else:
            risk = 'low'

        result.append((town, risk, deviations))

    return utils.sorted_by_key(result, 2, reverse=True)