from datetime import timedelta

from floodsystem import stationdata, datafetcher, flood, plot

def run():
    """Requirements for Task 2F"""

    all_stations = stationdata.build_station_list()
    stationdata.update_water_levels(all_stations)

    high_stations = flood.stations_highest_rel_level(all_stations, 5)

    for station, rel_level in high_stations:
        dates, levels = datafetcher.fetch_measure_levels(station.measure_id, dt=timedelta(days=2))

        plot.plot_water_level_with_fit(station, dates, levels, 4)

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()
