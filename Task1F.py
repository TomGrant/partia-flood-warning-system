from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.geo import stations_within_radius

def run():
    """Requirements for Task 1F"""

    # Populates list of stations
    stations = build_station_list()
    inconsistent_stations = inconsistent_typical_range_stations(stations)

    inconsistent_station_names = sorted(map(
        lambda station: station.name,
        inconsistent_stations))

    print(inconsistent_station_names)

if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()
