from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

def run():
    """Requirements for Task 1B"""

    CAMBRIDGE = (52.2053, 0.1218)

    # Populates the station list
    stations = build_station_list()
    stations_dists = stations_by_distance(stations, CAMBRIDGE)
    
    # Converts station object to readable output tuple
    def station_info(station_dist):
        station, dist = station_dist
        return station.name, station.town, dist

    # Gets output tuples from station info
    closest = list(map(station_info, stations_dists[:10]))
    furthest = list(map(station_info, stations_dists[-10:]))

    print("10 closest stations:")
    print(closest)

    print("10 furthest stations:")
    print(furthest)

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()