from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level

def run():
    """Requirements for Task 2C"""

    stations = build_station_list()
    update_water_levels(stations)

    stations_highest_lvl = stations_highest_rel_level(stations, 10)
    
    for station_tuple in stations_highest_lvl:
        print(station_tuple[0].name + " " + str(station_tuple[1]))

   
if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()

