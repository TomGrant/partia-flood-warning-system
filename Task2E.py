import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import plot_water_levels

def run():
    """Requirements for Task 2E"""

    all_stations = build_station_list()
    update_water_levels(all_stations)
    top_stations = stations_highest_rel_level(all_stations,5)

    for station_t in top_stations:
        station = station_t[0]
        dates, levels = fetch_measure_levels(station.measure_id, dt=timedelta(days=10))
        plot_water_levels(station, dates, levels)

if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()
