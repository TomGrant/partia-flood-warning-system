from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_treshold

def run():
    """Requirements for Task 2B"""

    stations = build_station_list()
    update_water_levels(stations)

    stations_over_tol = stations_level_over_treshold(stations, 0.8)

    for station_tuple in stations_over_tol:
        print(station_tuple[0].name + " " + str(station_tuple[1]))
   
if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()
